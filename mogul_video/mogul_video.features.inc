<?php
/**
 * @file
 * mogul_video.features.inc
 */

/**
 * Implements hook_views_api().
 */
function mogul_video_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_flag_default_flags().
 */
function mogul_video_flag_default_flags() {
  $flags = array();
  // Exported flag: "Favorite Videos".
  $flags['mogul_video_favorites'] = array(
    'content_type' => 'node',
    'title' => 'Favorite Videos',
    'global' => '0',
    'types' => array(
      0 => 'mogul_video',
    ),
    'flag_short' => 'Add to Favorites',
    'flag_long' => 'Add [node:title] to your favorites.',
    'flag_message' => '[node:title] has now been added to your favorites.',
    'unflag_short' => 'Remove from Favorites',
    'unflag_long' => 'Remove [node:title] from your favorites.',
    'unflag_message' => '[node:title] has now been removed from your favorites.',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '2',
      ),
      'unflag' => array(
        0 => '2',
      ),
    ),
    'weight' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_on_page' => 1,
    'show_on_teaser' => 1,
    'show_contextual_link' => 0,
    'i18n' => 0,
    'api_version' => 2,
    'module' => 'mogul_video',
    'locked' => array(
      0 => 'name',
    ),
  );
  return $flags;

}

/**
 * Implements hook_image_default_styles().
 */
function mogul_video_image_default_styles() {
  $styles = array();

  // Exported image style: mogul_video_featured_playlist.
  $styles['mogul_video_featured_playlist'] = array(
    'name' => 'mogul_video_featured_playlist',
    'effects' => array(
      15 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => '150',
          'height' => '110',
        ),
        'weight' => '1',
      ),
    ),
  );

  // Exported image style: mogul_video_featured_preview.
  $styles['mogul_video_featured_preview'] = array(
    'name' => 'mogul_video_featured_preview',
    'effects' => array(
      2 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => '1180',
          'height' => '663',
        ),
        'weight' => '1',
      ),
    ),
  );

  // Exported image style: mogul_video_grid_large_playlist.
  $styles['mogul_video_grid_large_playlist'] = array(
    'name' => 'mogul_video_grid_large_playlist',
    'effects' => array(
      4 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => '200',
          'height' => '112',
        ),
        'weight' => '1',
      ),
    ),
  );

  // Exported image style: mogul_video_grid_large_preview.
  $styles['mogul_video_grid_large_preview'] = array(
    'name' => 'mogul_video_grid_large_preview',
    'effects' => array(
      3 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => '1180',
          'height' => '663',
        ),
        'weight' => '1',
      ),
    ),
  );

  // Exported image style: mogul_video_grid_small_preview.
  $styles['mogul_video_grid_small_preview'] = array(
    'name' => 'mogul_video_grid_small_preview',
    'effects' => array(
      13 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => '383',
          'height' => '215',
        ),
        'weight' => '1',
      ),
    ),
  );

  // Exported image style: mogul_video_node_preview.
  $styles['mogul_video_node_preview'] = array(
    'name' => 'mogul_video_node_preview',
    'effects' => array(
      16 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => '1180',
          'height' => '663',
        ),
        'weight' => '1',
      ),
    ),
  );

  // Exported image style: mogul_video_player_2_player_playlist.
  $styles['mogul_video_player_2_player_playlist'] = array(
    'name' => 'mogul_video_player_2_player_playlist',
    'effects' => array(
      14 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => '270',
          'height' => '110',
        ),
        'weight' => '1',
      ),
    ),
  );

  // Exported image style: mogul_video_player_2_player_preview.
  $styles['mogul_video_player_2_player_preview'] = array(
    'name' => 'mogul_video_player_2_player_preview',
    'effects' => array(
      5 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => '1180',
          'height' => '663',
        ),
        'weight' => '1',
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_mediafront_default_presets().
 */
function mogul_video_mediafront_default_presets() {
  $items = array(
    'mogul_video_featured' => array(
      'name' => 'mogul_video_featured',
      'description' => 'The <b>Featured Video</b> player that displays a combined <b>player and playlist</b> for the <b>Video</b> content type.',
      'player' => 'osmplayer',
      'connect' => array(
        'iscontroller' => array(
          0 => '0',
        ),
        'isplaylist' => array(
          0 => '0',
        ),
      ),
      'settings' => array(
        'debug' => 0,
        'volume' => '80',
        'wmode' => 'transparent',
        'preload' => TRUE,
        'autoplay' => 0,
        'autoload' => 0,
        'scrollMode' => 'none',
        'scrollSpeed' => '20',
        'showPlaylist' => 1,
        'vertical' => '1',
        'node' => array(),
        'playlist' => '',
        'pageLimit' => '10',
        'preset' => '',
        'autoNext' => 1,
        'shuffle' => 0,
        'loop' => 0,
        'logo' => '/sites/all/modules/mediafront/players/osmplayer/player/logo.png',
        'swfplayer' => '/sites/all/modules/mediafront/players/osmplayer/player/minplayer/flash/minplayer.swf',
        'link' => 'http://www.mediafront.org',
        'width' => '',
        'height' => '',
        'template' => 'stretchy',
        'playlistOnly' => 0,
        'disablePlaylist' => 0,
        'controllerOnly' => 0,
        'volumeVertical' => 1,
        'theme' => 'dark-hive',
        'showWhenEmpty' => 1,
        'showController' => 1,
        'prereel' => '',
        'postreel' => '',
      ),
    ),
    'mogul_video_grid_large' => array(
      'name' => 'mogul_video_grid_large',
      'description' => 'The <b>large grid</b> player that displays a player in the <b>header</b> of the view for the <b>Video</b> content type.',
      'player' => 'osmplayer',
      'connect' => array(
        'iscontroller' => array(
          0 => '0',
        ),
        'isplaylist' => array(
          0 => '0',
        ),
      ),
      'settings' => array(
        'debug' => 0,
        'volume' => '80',
        'wmode' => 'transparent',
        'preload' => TRUE,
        'autoplay' => 0,
        'autoload' => 0,
        'scrollMode' => 'auto',
        'scrollSpeed' => '20',
        'showPlaylist' => 0,
        'vertical' => '1',
        'node' => array(),
        'playlist' => '',
        'pageLimit' => '10',
        'preset' => '',
        'autoNext' => 1,
        'shuffle' => 0,
        'loop' => 0,
        'logo' => '/sites/all/modules/mediafront/players/osmplayer/player/logo.png',
        'swfplayer' => '/sites/all/modules/mediafront/players/osmplayer/player/minplayer/flash/minplayer.swf',
        'link' => 'http://www.mediafront.org',
        'width' => '',
        'height' => '',
        'template' => 'stretchy',
        'playlistOnly' => 0,
        'disablePlaylist' => 0,
        'controllerOnly' => 0,
        'volumeVertical' => 1,
        'theme' => 'dark-hive',
        'showWhenEmpty' => 1,
        'showController' => 1,
        'prereel' => '',
        'postreel' => '',
      ),
    ),
    'mogul_video_grid_small' => array(
      'name' => 'mogul_video_grid_small',
      'description' => 'The <b>small grid</b> player that displays a player for <b>each node</b> in the view  for the <b>Video</b> content type.',
      'player' => 'osmplayer',
      'connect' => array(
        'iscontroller' => array(
          0 => '0',
        ),
        'isplaylist' => array(
          0 => '0',
        ),
      ),
      'settings' => array(
        'debug' => 0,
        'volume' => '80',
        'wmode' => 'transparent',
        'preload' => TRUE,
        'autoplay' => 0,
        'autoload' => 0,
        'scrollMode' => 'auto',
        'scrollSpeed' => '20',
        'showPlaylist' => 0,
        'vertical' => '1',
        'node' => array(),
        'playlist' => '',
        'pageLimit' => '10',
        'preset' => '',
        'autoNext' => 0,
        'shuffle' => 0,
        'loop' => 0,
        'logo' => '/sites/all/modules/mediafront/players/osmplayer/player/logo.png',
        'swfplayer' => '/sites/all/modules/mediafront/players/osmplayer/player/minplayer/flash/minplayer.swf',
        'link' => 'http://www.mediafront.org',
        'width' => '',
        'height' => '',
        'template' => 'stretchy',
        'playlistOnly' => 0,
        'disablePlaylist' => 0,
        'controllerOnly' => 0,
        'volumeVertical' => 1,
        'plugins' => array(
          'timeline_indicator' => 1,
        ),
        'theme' => 'dark-hive',
        'showWhenEmpty' => 0,
        'showController' => 1,
        'prereel' => '',
        'postreel' => '',
        'player_settings__active_tab' => 'edit-player-settings-plugins',
      ),
    ),
    'mogul_video_node' => array(
      'name' => 'mogul_video_node',
      'description' => 'The <b>node</b> player for the <b>Video</b> content type.',
      'player' => 'osmplayer',
      'connect' => array(
        'iscontroller' => array(
          0 => '0',
        ),
        'isplaylist' => array(
          0 => '0',
        ),
      ),
      'settings' => array(
        'debug' => 0,
        'volume' => '80',
        'wmode' => 'transparent',
        'preload' => TRUE,
        'autoplay' => 0,
        'autoload' => 0,
        'scrollMode' => 'auto',
        'scrollSpeed' => '20',
        'showPlaylist' => 1,
        'vertical' => '1',
        'node' => array(),
        'playlist' => '',
        'pageLimit' => '10',
        'preset' => '',
        'autoNext' => 0,
        'shuffle' => 0,
        'loop' => 0,
        'logo' => '/sites/all/modules/mediafront/players/osmplayer/player/logo.png',
        'swfplayer' => '/sites/all/modules/mediafront/players/osmplayer/player/minplayer/flash/minplayer.swf',
        'link' => 'http://www.mediafront.org',
        'width' => '',
        'height' => '',
        'template' => 'stretchy',
        'playlistOnly' => 0,
        'disablePlaylist' => 0,
        'controllerOnly' => 0,
        'volumeVertical' => 1,
        'theme' => 'dark-hive',
        'showWhenEmpty' => 0,
        'showController' => 1,
        'prereel' => '',
        'postreel' => '',
      ),
    ),
    'mogul_video_player_2_player_player' => array(
      'name' => 'mogul_video_player_2_player_player',
      'description' => 'The <b>player</b> that connects to the playlist only preset for the <b>Video</b> content type.',
      'player' => 'osmplayer',
      'connect' => array(
        'iscontroller' => array(
          'mogul_video_player_2_player_playlist' => 'mogul_video_player_2_player_playlist',
        ),
        'isplaylist' => array(
          0 => '0',
        ),
      ),
      'settings' => array(
        'debug' => 0,
        'volume' => '80',
        'wmode' => 'transparent',
        'preload' => TRUE,
        'autoplay' => 0,
        'autoload' => 0,
        'scrollMode' => 'auto',
        'scrollSpeed' => '20',
        'showPlaylist' => 0,
        'vertical' => '1',
        'node' => array(),
        'playlist' => '',
        'pageLimit' => '10',
        'preset' => '',
        'autoNext' => 1,
        'shuffle' => 0,
        'loop' => 0,
        'logo' => '/sites/all/modules/mediafront/players/osmplayer/player/logo.png',
        'swfplayer' => '/sites/all/modules/mediafront/players/osmplayer/player/minplayer/flash/minplayer.swf',
        'link' => 'http://www.mediafront.org',
        'width' => '',
        'height' => '',
        'template' => 'stretchy',
        'playlistOnly' => 0,
        'disablePlaylist' => 1,
        'controllerOnly' => 0,
        'volumeVertical' => 1,
        'theme' => 'dark-hive',
        'showWhenEmpty' => 1,
        'showController' => 1,
        'prereel' => '',
        'postreel' => '',
      ),
    ),
    'mogul_video_player_2_player_playlist' => array(
      'name' => 'mogul_video_player_2_player_playlist',
      'description' => 'The <b>playlist</b> that connects to the player only preset for the <b>Video</b> content type.',
      'player' => 'osmplayer',
      'connect' => array(
        'iscontroller' => array(
          0 => '0',
        ),
        'isplaylist' => array(
          'mogul_video_player_2_player_player' => 'mogul_video_player_2_player_player',
        ),
      ),
      'settings' => array(
        'debug' => 0,
        'volume' => '80',
        'wmode' => 'transparent',
        'preload' => TRUE,
        'autoplay' => 0,
        'autoload' => 1,
        'scrollMode' => 'auto',
        'scrollSpeed' => '20',
        'showPlaylist' => 1,
        'vertical' => '1',
        'node' => array(),
        'playlist' => '',
        'pageLimit' => '10',
        'preset' => '',
        'autoNext' => 1,
        'shuffle' => 0,
        'loop' => 0,
        'logo' => '/sites/all/modules/mediafront/players/osmplayer/player/logo.png',
        'swfplayer' => '/sites/all/modules/mediafront/players/osmplayer/player/minplayer/flash/minplayer.swf',
        'link' => 'http://www.mediafront.org',
        'width' => '',
        'height' => '',
        'template' => 'stretchy',
        'playlistOnly' => 1,
        'disablePlaylist' => 0,
        'controllerOnly' => 0,
        'volumeVertical' => 1,
        'theme' => 'dark-hive',
        'showWhenEmpty' => 1,
        'showController' => 1,
        'prereel' => '',
        'postreel' => '',
      ),
    ),
  );
  return $items;
}

/**
 * Implements hook_mediafront_views_default_options().
 */
function mogul_video_mediafront_views_default_options() {
  $options = array(
    'mogul_video_favorites' => array(
      'ops' => array(
        'link_to_player' => 0,
        'field_type' => '0',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'title' => array(
        'link_to_player' => 0,
        'field_type' => '0',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'field_mogul_video_5star_rating' => array(
        'link_to_player' => 0,
        'field_type' => '0',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'uid' => array(
        'link_to_player' => 0,
        'field_type' => '0',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
    ),
    'mogul_video_featured' => array(
      'title' => array(
        'link_to_player' => 0,
        'field_type' => 'title',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'field_mogul_video_image' => array(
        'link_to_player' => 0,
        'field_type' => 'image',
        'media_type' => 'media',
        'preview' => 'mogul_video_featured_preview',
        'thumbnail' => 'mogul_video_featured_playlist',
        'custom' => '',
      ),
      'field_mogul_video_file' => array(
        'link_to_player' => 0,
        'field_type' => 'media',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'field_mogul_video_embed_url' => array(
        'link_to_player' => 0,
        'field_type' => 'media',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'field_mogul_video_remote_file' => array(
        'link_to_player' => 0,
        'field_type' => 'media',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
    ),
    'mogul_video_grid_large' => array(
      'field_mogul_video_image' => array(
        'link_to_player' => 1,
        'field_type' => 'image',
        'media_type' => 'media',
        'preview' => 'mogul_video_grid_large_preview',
        'thumbnail' => 'mogul_video_grid_large_playlist',
        'custom' => '',
      ),
      'field_mogul_video_file' => array(
        'link_to_player' => 1,
        'field_type' => 'media',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'field_mogul_video_embed_url' => array(
        'link_to_player' => 1,
        'field_type' => 'media',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'field_mogul_video_remote_file' => array(
        'link_to_player' => 1,
        'field_type' => 'media',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'title' => array(
        'link_to_player' => 0,
        'field_type' => 'title',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'nid' => array(
        'link_to_player' => 0,
        'field_type' => '0',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'field_mogul_video_5star' => array(
        'link_to_player' => 0,
        'field_type' => '0',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'ops' => array(
        'link_to_player' => 0,
        'field_type' => '0',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
    ),
    'mogul_video_grid_small' => array(
      'field_mogul_video_file' => array(
        'link_to_player' => 0,
        'field_type' => 'media',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'title' => array(
        'link_to_player' => 0,
        'field_type' => 'title',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'field_mogul_video_image' => array(
        'link_to_player' => 0,
        'field_type' => 'image',
        'media_type' => 'media',
        'preview' => 'mogul_video_grid_small_preview',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'field_mogul_video_embed_url' => array(
        'link_to_player' => 0,
        'field_type' => 'media',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'field_mogul_video_remote_file' => array(
        'link_to_player' => 0,
        'field_type' => 'media',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'nid' => array(
        'link_to_player' => 0,
        'field_type' => '0',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'field_mogul_video_5star' => array(
        'link_to_player' => 0,
        'field_type' => '0',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'ops' => array(
        'link_to_player' => 0,
        'field_type' => '0',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
    ),
    'mogul_video_player_2_player' => array(
      'title' => array(
        'link_to_player' => 0,
        'field_type' => '0',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'field_mogul_video_image' => array(
        'link_to_player' => 0,
        'field_type' => 'image',
        'media_type' => 'media',
        'preview' => 'mogul_video_player_2_player_preview',
        'thumbnail' => 'mogul_video_player_2_player_playlist',
        'custom' => '',
      ),
      'field_mogul_video_embed_url' => array(
        'link_to_player' => 0,
        'field_type' => 'media',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'field_mogul_video_remote_file' => array(
        'link_to_player' => 0,
        'field_type' => 'media',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'field_mogul_video_file' => array(
        'link_to_player' => 0,
        'field_type' => '0',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
    ),
  );
  return $options;
}

/**
 * Implements hook_node_info().
 */
function mogul_video_node_info() {
  $items = array(
    'mogul_video' => array(
      'name' => t('Video'),
      'base' => 'node_content',
      'description' => t('Use <em>Video</em> to upload a video file for your users to watch in a media player.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => t('<strong>Important:</strong> To add video, you have 1 of 3 options. Choose only 1 option per page (i.e. You cannot have YouTube and Remote Files in the same player on the same page.'),
    ),
  );
  return $items;
}
