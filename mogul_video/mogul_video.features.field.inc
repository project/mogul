<?php
/**
 * @file
 * mogul_video.features.field.inc
 */

/**
 * Implements hook_field_default_fields().
 */
function mogul_video_field_default_fields() {
  $fields = array();

  // Exported field: 'node-mogul_video-body'.
  $fields['node-mogul_video-body'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(
        0 => 'node',
      ),
      'field_name' => 'body',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'locked' => '0',
      'module' => 'text',
      'settings' => array(),
      'translatable' => '0',
      'type' => 'text_with_summary',
    ),
    'field_instance' => array(
      'bundle' => 'mogul_video',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => '4',
        ),
        'teaser' => array(
          'label' => 'hidden',
          'module' => 'text',
          'settings' => array(
            'trim_length' => 600,
          ),
          'type' => 'text_summary_or_trimmed',
          'weight' => '0',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'body',
      'label' => 'Body',
      'required' => FALSE,
      'settings' => array(
        'display_summary' => TRUE,
        'text_processing' => 1,
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'module' => 'text',
        'settings' => array(
          'rows' => 20,
          'summary_rows' => 5,
        ),
        'type' => 'text_textarea_with_summary',
        'weight' => '6',
      ),
    ),
  );

  // Exported field: 'node-mogul_video-field_mogul_video_5star'.
  $fields['node-mogul_video-field_mogul_video_5star'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_mogul_video_5star',
      'foreign keys' => array(),
      'indexes' => array(),
      'locked' => '0',
      'module' => 'fivestar',
      'settings' => array(
        'axis' => 'vote',
      ),
      'translatable' => '0',
      'type' => 'fivestar',
    ),
    'field_instance' => array(
      'bundle' => 'mogul_video',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'inline',
          'module' => 'fivestar',
          'settings' => array(
            'expose' => 1,
            'style' => 'average',
            'text' => 'none',
            'widget' => array(
              'fivestar_widget' => 'sites/all/modules/fivestar/widgets/oxygen/oxygen.css',
            ),
          ),
          'type' => 'fivestar_formatter_default',
          'weight' => '3',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_mogul_video_5star',
      'label' => 'Rating',
      'required' => 0,
      'settings' => array(
        'allow_clear' => 0,
        'mediafront' => array(
          'custom' => 'field_mogul_video_5star',
          'field_type' => 'custom',
          'media_type' => 'media',
          'preview' => '0',
          'thumbnail' => '0',
        ),
        'stars' => '5',
        'target' => 'none',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'fivestar',
        'settings' => array(),
        'type' => 'exposed',
        'weight' => '5',
      ),
    ),
  );

  // Exported field: 'node-mogul_video-field_mogul_video_embed_url'.
  $fields['node-mogul_video-field_mogul_video_embed_url'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_mogul_video_embed_url',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'locked' => '0',
      'module' => 'text',
      'settings' => array(
        'max_length' => '1000',
      ),
      'translatable' => '0',
      'type' => 'text',
    ),
    'field_instance' => array(
      'bundle' => 'mogul_video',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => 'Embed a <a href="http://www.youtube.com/" target="_blank">YouTube</a> or <a href="http://vimeo.com/" target="_blank">Vimeo</a> video. Copy and paste link (URL) to the video. e.g. http://www.youtube.com/watch?v=i-DYHEuRCBA (Note: Do not use the embed code).',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'mediafront',
          'settings' => array(
            'preset' => 'mogul_video_node',
          ),
          'type' => 'mediafront_player',
          'weight' => '1',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '2',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_mogul_video_embed_url',
      'label' => 'Option 2: Video Embed URL',
      'required' => 0,
      'settings' => array(
        'mediafront' => array(
          'custom' => '',
          'field_type' => 'media',
          'media_type' => 'media',
          'preview' => '0',
          'thumbnail' => '0',
        ),
        'text_processing' => '0',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'size' => '60',
        ),
        'type' => 'text_textfield',
        'weight' => '3',
      ),
    ),
  );

  // Exported field: 'node-mogul_video-field_mogul_video_file'.
  $fields['node-mogul_video-field_mogul_video_file'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '3',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_mogul_video_file',
      'foreign keys' => array(
        'fid' => array(
          'columns' => array(
            'fid' => 'fid',
          ),
          'table' => 'file_managed',
        ),
      ),
      'indexes' => array(
        'fid' => array(
          0 => 'fid',
        ),
      ),
      'locked' => '0',
      'module' => 'file',
      'settings' => array(
        'display_default' => 0,
        'display_field' => 0,
        'uri_scheme' => 'public',
      ),
      'translatable' => '0',
      'type' => 'file',
    ),
    'field_instance' => array(
      'bundle' => 'mogul_video',
      'deleted' => '0',
      'description' => 'For your video to be accessible to as many browsers/devices as possible, you will have to upload your video file in 3 formats: .webm .ogv and .mp4.  Please upload them is this order. See <a href="http://drupal.org/node/1565532" target="_blank">HTML 5 Video File Formats and Encoding</a> for more information.',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'mediafront',
          'settings' => array(
            'preset' => 'mogul_video_node',
          ),
          'type' => 'mediafront_player',
          'weight' => '0',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '1',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_mogul_video_file',
      'label' => 'Option 1: Video File',
      'required' => 0,
      'settings' => array(
        'description_field' => 0,
        'file_directory' => '',
        'file_extensions' => 'mp4 webm ogv',
        'max_filesize' => '',
        'mediafront' => array(
          'custom' => '',
          'field_type' => 'media',
          'media_type' => 'media',
          'preview' => '0',
          'thumbnail' => '0',
        ),
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'file',
        'settings' => array(
          'imce_filefield_on' => 1,
          'progress_indicator' => 'bar',
        ),
        'type' => 'file_generic',
        'weight' => '2',
      ),
    ),
  );

  // Exported field: 'node-mogul_video-field_mogul_video_image'.
  $fields['node-mogul_video-field_mogul_video_image'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_mogul_video_image',
      'foreign keys' => array(
        'fid' => array(
          'columns' => array(
            'fid' => 'fid',
          ),
          'table' => 'file_managed',
        ),
      ),
      'indexes' => array(
        'fid' => array(
          0 => 'fid',
        ),
      ),
      'locked' => '0',
      'module' => 'image',
      'settings' => array(
        'default_image' => 0,
        'uri_scheme' => 'public',
      ),
      'translatable' => '0',
      'type' => 'image',
    ),
    'field_instance' => array(
      'bundle' => 'mogul_video',
      'deleted' => '0',
      'description' => 'This is the preview image used in the player before the video starts or in playlists. Images are scaled based on a 16:9 aspect ratio. Upload images that closely resemble this aspect ration for better results.',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '5',
        ),
        'teaser' => array(
          'label' => 'hidden',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '4',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_mogul_video_image',
      'label' => 'Video Image',
      'required' => 1,
      'settings' => array(
        'alt_field' => 0,
        'default_image' => 0,
        'file_directory' => '',
        'file_extensions' => 'png gif jpg jpeg',
        'max_filesize' => '',
        'max_resolution' => '',
        'mediafront' => array(
          'custom' => '',
          'field_type' => 'image',
          'media_type' => 'media',
          'preview' => 'mogul_video_node_preview',
          'thumbnail' => '0',
        ),
        'min_resolution' => '1180x663',
        'title_field' => 0,
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'image',
        'settings' => array(
          'imce_filefield_on' => 1,
          'preview_image_style' => 'thumbnail',
          'progress_indicator' => 'throbber',
        ),
        'type' => 'image_image',
        'weight' => '1',
      ),
    ),
  );

  // Exported field: 'node-mogul_video-field_mogul_video_remote_file'.
  $fields['node-mogul_video-field_mogul_video_remote_file'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '3',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_mogul_video_remote_file',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'locked' => '0',
      'module' => 'text',
      'settings' => array(
        'max_length' => '500',
      ),
      'translatable' => '0',
      'type' => 'text',
    ),
    'field_instance' => array(
      'bundle' => 'mogul_video',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => 'Provide a URL to any file(s) on a remote server (e.g. http://www.another-server/your-file.mp4). For your video to be accessible to as many browsers/devices as possible, you will have to provide your video file in 3 formats: .webm .ogv and .mp4.  Please link to them is this order. See <a href="http://drupal.org/node/1565532" target="_blank">HTML 5 Video File Formats and Encoding</a> for more information.',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'mediafront',
          'settings' => array(
            'preset' => 'mogul_video_node',
          ),
          'type' => 'mediafront_player',
          'weight' => '2',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '3',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_mogul_video_remote_file',
      'label' => 'Option 3: Video Remote File',
      'required' => 0,
      'settings' => array(
        'mediafront' => array(
          'custom' => '',
          'field_type' => 'media',
          'media_type' => 'media',
          'preview' => '0',
          'thumbnail' => '0',
        ),
        'text_processing' => '0',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'size' => '60',
        ),
        'type' => 'text_textfield',
        'weight' => '4',
      ),
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Body');
  t('Embed a <a href="http://www.youtube.com/" target="_blank">YouTube</a> or <a href="http://vimeo.com/" target="_blank">Vimeo</a> video. Copy and paste link (URL) to the video. e.g. http://www.youtube.com/watch?v=i-DYHEuRCBA (Note: Do not use the embed code).');
  t('For your video to be accessible to as many browsers/devices as possible, you will have to upload your video file in 3 formats: .webm .ogv and .mp4.  Please upload them is this order. See <a href="http://drupal.org/node/1565532" target="_blank">HTML 5 Video File Formats and Encoding</a> for more information.');
  t('Option 1: Video File');
  t('Option 2: Video Embed URL');
  t('Option 3: Video Remote File');
  t('Provide a URL to any file(s) on a remote server (e.g. http://www.another-server/your-file.mp4). For your video to be accessible to as many browsers/devices as possible, you will have to provide your video file in 3 formats: .webm .ogv and .mp4.  Please link to them is this order. See <a href="http://drupal.org/node/1565532" target="_blank">HTML 5 Video File Formats and Encoding</a> for more information.');
  t('Rating');
  t('This is the preview image used in the player before the video starts or in playlists. Images are scaled based on a 16:9 aspect ratio. Upload images that closely resemble this aspect ration for better results.');
  t('Video Image');

  return $fields;
}
