<?php
/**
 * @file
 * mogul_podcast.features.inc
 */

/**
 * Implements hook_views_api().
 */
function mogul_podcast_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_flag_default_flags().
 */
function mogul_podcast_flag_default_flags() {
  $flags = array();
  // Exported flag: "Favorite Podcasts".
  $flags['mogul_podcast_favorites'] = array(
    'content_type' => 'node',
    'title' => 'Favorite Podcasts',
    'global' => '0',
    'types' => array(
      0 => 'mogul_podcast',
    ),
    'flag_short' => 'Add to Favorites',
    'flag_long' => 'Add [node:title] to your favorites.',
    'flag_message' => '[node:title] has now been added to your favorites.',
    'unflag_short' => 'Remove from Favorites',
    'unflag_long' => 'Remove [node:title] from your favorites.',
    'unflag_message' => '[node:title] has now been removed from your favorites.',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '2',
      ),
      'unflag' => array(
        0 => '2',
      ),
    ),
    'weight' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_on_page' => 1,
    'show_on_teaser' => 1,
    'show_contextual_link' => 0,
    'i18n' => 0,
    'api_version' => 2,
    'module' => 'mogul_podcast',
    'locked' => array(
      0 => 'name',
    ),
  );
  return $flags;

}

/**
 * Implements hook_image_default_styles().
 */
function mogul_podcast_image_default_styles() {
  $styles = array();

  // Exported image style: mogul_podcast_featured_playlist.
  $styles['mogul_podcast_featured_playlist'] = array(
    'name' => 'mogul_podcast_featured_playlist',
    'effects' => array(
      7 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => '110',
          'height' => '110',
        ),
        'weight' => '1',
      ),
    ),
  );

  // Exported image style: mogul_podcast_featured_preview.
  $styles['mogul_podcast_featured_preview'] = array(
    'name' => 'mogul_podcast_featured_preview',
    'effects' => array(
      6 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => '664',
          'height' => '664',
        ),
        'weight' => '1',
      ),
    ),
  );

  // Exported image style: mogul_podcast_grid_large_playlist.
  $styles['mogul_podcast_grid_large_playlist'] = array(
    'name' => 'mogul_podcast_grid_large_playlist',
    'effects' => array(
      9 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => '200',
          'height' => '200',
        ),
        'weight' => '1',
      ),
    ),
  );

  // Exported image style: mogul_podcast_grid_large_preview.
  $styles['mogul_podcast_grid_large_preview'] = array(
    'name' => 'mogul_podcast_grid_large_preview',
    'effects' => array(
      8 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => '664',
          'height' => '664',
        ),
        'weight' => '1',
      ),
    ),
  );

  // Exported image style: mogul_podcast_grid_small_preview.
  $styles['mogul_podcast_grid_small_preview'] = array(
    'name' => 'mogul_podcast_grid_small_preview',
    'effects' => array(
      10 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => '216',
          'height' => '216',
        ),
        'weight' => '1',
      ),
    ),
  );

  // Exported image style: mogul_podcast_itunes.
  $styles['mogul_podcast_itunes'] = array(
    'name' => 'mogul_podcast_itunes',
    'effects' => array(
      16 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => '1400',
          'height' => '1400',
        ),
        'weight' => '1',
      ),
    ),
  );

  // Exported image style: mogul_podcast_node_preview.
  $styles['mogul_podcast_node_preview'] = array(
    'name' => 'mogul_podcast_node_preview',
    'effects' => array(
      17 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => '664',
          'height' => '664',
        ),
        'weight' => '1',
      ),
    ),
  );

  // Exported image style: mogul_podcast_player_2_player_playlist.
  $styles['mogul_podcast_player_2_player_playlist'] = array(
    'name' => 'mogul_podcast_player_2_player_playlist',
    'effects' => array(
      11 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => '110',
          'height' => '110',
        ),
        'weight' => '1',
      ),
    ),
  );

  // Exported image style: mogul_podcast_player_2_player_preview.
  $styles['mogul_podcast_player_2_player_preview'] = array(
    'name' => 'mogul_podcast_player_2_player_preview',
    'effects' => array(
      12 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => '664',
          'height' => '664',
        ),
        'weight' => '1',
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_mediafront_default_presets().
 */
function mogul_podcast_mediafront_default_presets() {
  $items = array(
    'mogul_podcast_featured' => array(
      'name' => 'mogul_podcast_featured',
      'description' => 'The <b>Featured Podcast</b> player that displays a combined <b>player and playlist</b> for the <b>Podcast</b> content type.',
      'player' => 'osmplayer',
      'connect' => array(
        'iscontroller' => array(
          0 => '0',
        ),
        'isplaylist' => array(
          0 => '0',
        ),
      ),
      'settings' => array(
        'debug' => 0,
        'volume' => '80',
        'wmode' => 'transparent',
        'preload' => TRUE,
        'autoplay' => 0,
        'autoload' => 0,
        'scrollMode' => 'auto',
        'scrollSpeed' => '20',
        'showPlaylist' => 1,
        'vertical' => '1',
        'node' => array(),
        'playlist' => '',
        'pageLimit' => '10',
        'preset' => '',
        'autoNext' => 1,
        'shuffle' => 0,
        'loop' => 0,
        'logo' => '/sites/all/modules/mediafront/players/osmplayer/player/logo.png',
        'swfplayer' => '/sites/all/modules/mediafront/players/osmplayer/player/minplayer/flash/minplayer.swf',
        'link' => 'http://www.mediafront.org',
        'width' => '',
        'height' => '',
        'template' => 'stretchy',
        'playlistOnly' => 0,
        'disablePlaylist' => 0,
        'controllerOnly' => 0,
        'volumeVertical' => 1,
        'theme' => 'dark-hive',
        'showWhenEmpty' => 1,
        'showController' => 1,
        'prereel' => '',
        'postreel' => '',
      ),
    ),
    'mogul_podcast_grid_large' => array(
      'name' => 'mogul_podcast_grid_large',
      'description' => 'The <b>large grid</b> player that displays a player in the <b>header</b> of the view for the <b>Podcast</b> content type.',
      'player' => 'osmplayer',
      'connect' => array(
        'iscontroller' => array(
          0 => '0',
        ),
        'isplaylist' => array(
          0 => '0',
        ),
      ),
      'settings' => array(
        'debug' => 0,
        'volume' => '80',
        'wmode' => 'transparent',
        'preload' => TRUE,
        'autoplay' => 0,
        'autoload' => 0,
        'scrollMode' => 'auto',
        'scrollSpeed' => '20',
        'showPlaylist' => 0,
        'vertical' => '1',
        'node' => array(),
        'playlist' => '',
        'pageLimit' => '10',
        'preset' => '',
        'autoNext' => 1,
        'shuffle' => 0,
        'loop' => 0,
        'logo' => '/sites/all/modules/mediafront/players/osmplayer/player/logo.png',
        'swfplayer' => '/sites/all/modules/mediafront/players/osmplayer/player/minplayer/flash/minplayer.swf',
        'link' => 'http://www.mediafront.org',
        'width' => '',
        'height' => '',
        'template' => 'stretchy',
        'playlistOnly' => 0,
        'disablePlaylist' => 0,
        'controllerOnly' => 0,
        'volumeVertical' => 1,
        'theme' => 'dark-hive',
        'showWhenEmpty' => 1,
        'showController' => 1,
        'prereel' => '',
        'postreel' => '',
      ),
    ),
    'mogul_podcast_grid_small' => array(
      'name' => 'mogul_podcast_grid_small',
      'description' => 'The <b>small grid</b> player that displays a player for <b>each node</b> in the view  for the <b>Podcast</b> content type.',
      'player' => 'osmplayer',
      'connect' => array(
        'iscontroller' => array(
          0 => '0',
        ),
        'isplaylist' => array(
          0 => '0',
        ),
      ),
      'settings' => array(
        'debug' => 0,
        'volume' => '80',
        'wmode' => 'transparent',
        'preload' => TRUE,
        'autoplay' => 0,
        'autoload' => 0,
        'scrollMode' => 'auto',
        'scrollSpeed' => '20',
        'showPlaylist' => 1,
        'vertical' => '1',
        'node' => array(),
        'playlist' => '',
        'pageLimit' => '10',
        'preset' => '',
        'autoNext' => 1,
        'shuffle' => 0,
        'loop' => 0,
        'logo' => '/sites/all/modules/mediafront/players/osmplayer/player/logo.png',
        'swfplayer' => '/sites/all/modules/mediafront/players/osmplayer/player/minplayer/flash/minplayer.swf',
        'link' => 'http://www.mediafront.org',
        'width' => '',
        'height' => '',
        'template' => 'stretchy',
        'playlistOnly' => 0,
        'disablePlaylist' => 0,
        'controllerOnly' => 0,
        'volumeVertical' => 1,
        'plugins' => array(
          'timeline_indicator' => 0,
        ),
        'theme' => 'dark-hive',
        'showWhenEmpty' => 0,
        'showController' => 1,
        'prereel' => '',
        'postreel' => '',
        'player_settings__active_tab' => 'edit-player-settings-plugins',
      ),
    ),
    'mogul_podcast_node' => array(
      'name' => 'mogul_podcast_node',
      'description' => 'The <b>node</b> player for the <b>Podcast</b> content type.',
      'player' => 'osmplayer',
      'connect' => array(
        'iscontroller' => array(
          0 => '0',
        ),
        'isplaylist' => array(
          0 => '0',
        ),
      ),
      'settings' => array(
        'debug' => 0,
        'volume' => '80',
        'wmode' => 'transparent',
        'preload' => TRUE,
        'autoplay' => 0,
        'autoload' => 0,
        'scrollMode' => 'auto',
        'scrollSpeed' => '20',
        'showPlaylist' => 0,
        'vertical' => '1',
        'node' => array(),
        'playlist' => '',
        'pageLimit' => '10',
        'preset' => '',
        'autoNext' => 0,
        'shuffle' => 0,
        'loop' => 0,
        'logo' => '/sites/all/modules/mediafront/players/osmplayer/player/logo.png',
        'swfplayer' => '/sites/all/modules/mediafront/players/osmplayer/player/minplayer/flash/minplayer.swf',
        'link' => 'http://www.mediafront.org',
        'width' => '',
        'height' => '',
        'template' => 'stretchy',
        'playlistOnly' => 0,
        'disablePlaylist' => 0,
        'controllerOnly' => 0,
        'volumeVertical' => 1,
        'theme' => 'dark-hive',
        'showWhenEmpty' => 0,
        'showController' => 1,
        'prereel' => '',
        'postreel' => '',
      ),
    ),
    'mogul_podcast_player_2_player_player' => array(
      'name' => 'mogul_podcast_player_2_player_player',
      'description' => 'The <b>player</b> that connects to the playlist only preset for the <b>Podcast</b> content type.',
      'player' => 'osmplayer',
      'connect' => array(
        'iscontroller' => array(
          'mogul_podcast_player_2_player_playlist' => 'mogul_podcast_player_2_player_playlist',
        ),
        'isplaylist' => array(
          0 => '0',
        ),
      ),
      'settings' => array(
        'debug' => 0,
        'volume' => '80',
        'wmode' => 'transparent',
        'preload' => TRUE,
        'autoplay' => 0,
        'autoload' => 0,
        'scrollMode' => 'auto',
        'scrollSpeed' => '20',
        'showPlaylist' => 0,
        'vertical' => '1',
        'node' => array(),
        'playlist' => '',
        'pageLimit' => '10',
        'preset' => '',
        'autoNext' => 1,
        'shuffle' => 0,
        'loop' => 0,
        'logo' => '/sites/all/modules/mediafront/players/osmplayer/player/logo.png',
        'swfplayer' => '/sites/all/modules/mediafront/players/osmplayer/player/minplayer/flash/minplayer.swf',
        'link' => 'http://www.mediafront.org',
        'width' => '',
        'height' => '',
        'template' => 'stretchy',
        'playlistOnly' => 0,
        'disablePlaylist' => 1,
        'controllerOnly' => 0,
        'volumeVertical' => 1,
        'theme' => 'dark-hive',
        'showWhenEmpty' => 0,
        'showController' => 1,
        'prereel' => '',
        'postreel' => '',
      ),
    ),
    'mogul_podcast_player_2_player_playlist' => array(
      'name' => 'mogul_podcast_player_2_player_playlist',
      'description' => 'The <b>playlist</b> that connects to the player only preset for the <b>Podcast</b> content type.',
      'player' => 'osmplayer',
      'connect' => array(
        'iscontroller' => array(
          0 => '0',
        ),
        'isplaylist' => array(
          'mogul_podcast_player_2_player_player' => 'mogul_podcast_player_2_player_player',
        ),
      ),
      'settings' => array(
        'debug' => 0,
        'volume' => '80',
        'wmode' => 'transparent',
        'preload' => TRUE,
        'autoplay' => 0,
        'autoload' => 1,
        'scrollMode' => 'auto',
        'scrollSpeed' => '20',
        'showPlaylist' => 1,
        'vertical' => '1',
        'node' => array(),
        'playlist' => '',
        'pageLimit' => '10',
        'preset' => '',
        'autoNext' => 1,
        'shuffle' => 0,
        'loop' => 0,
        'logo' => '/sites/all/modules/mediafront/players/osmplayer/player/logo.png',
        'swfplayer' => '/sites/all/modules/mediafront/players/osmplayer/player/minplayer/flash/minplayer.swf',
        'link' => 'http://www.mediafront.org',
        'width' => '100%',
        'height' => '500px',
        'template' => 'default',
        'playlistOnly' => 1,
        'disablePlaylist' => 0,
        'controllerOnly' => 0,
        'theme' => 'dark-hive',
        'showWhenEmpty' => 1,
        'showController' => 1,
        'prereel' => '',
        'postreel' => '',
      ),
    ),
  );
  return $items;
}

/**
 * Implements hook_mediafront_views_default_options().
 */
function mogul_podcast_mediafront_views_default_options() {
  $options = array(
    'mogul_podcast_favorites' => array(
      'uid' => array(
        'link_to_player' => 0,
        'field_type' => '0',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
    ),
    'mogul_podcast_featured' => array(
      'field_mogul_podcast_artwork' => array(
        'link_to_player' => 0,
        'field_type' => 'image',
        'media_type' => 'media',
        'preview' => 'mogul_podcast_featured_preview',
        'thumbnail' => 'mogul_podcast_featured_playlist',
        'custom' => '',
      ),
      'field_mogul_podcast_file' => array(
        'link_to_player' => 0,
        'field_type' => 'media',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'title' => array(
        'link_to_player' => 0,
        'field_type' => 'title',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
    ),
    'mogul_podcast_grid_large' => array(
      'field_mogul_podcast_artwork' => array(
        'link_to_player' => 1,
        'field_type' => 'image',
        'media_type' => 'media',
        'preview' => 'mogul_podcast_grid_large_preview',
        'thumbnail' => 'mogul_podcast_grid_large_playlist',
        'custom' => '',
      ),
      'field_mogul_podcast_file' => array(
        'link_to_player' => 0,
        'field_type' => 'media',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'title' => array(
        'link_to_player' => 0,
        'field_type' => 'title',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'nid' => array(
        'link_to_player' => 0,
        'field_type' => '0',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'ops' => array(
        'link_to_player' => 0,
        'field_type' => '0',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'field_mogul_podcast_5star' => array(
        'link_to_player' => 0,
        'field_type' => '0',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
    ),
    'mogul_podcast_grid_small' => array(
      'field_mogul_podcast_artwork' => array(
        'link_to_player' => 0,
        'field_type' => 'image',
        'media_type' => 'media',
        'preview' => 'mogul_podcast_grid_small_preview',
        'thumbnail' => 'mogul_podcast_grid_small_preview',
        'custom' => '',
      ),
      'field_mogul_podcast_file' => array(
        'link_to_player' => 0,
        'field_type' => 'media',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'title' => array(
        'link_to_player' => 0,
        'field_type' => 'title',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'nid' => array(
        'link_to_player' => 0,
        'field_type' => '0',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'field_mogul_podcast_5star' => array(
        'link_to_player' => 0,
        'field_type' => '0',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'ops' => array(
        'link_to_player' => 0,
        'field_type' => '0',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
    ),
    'mogul_podcast_player_2_player' => array(
      'field_mogul_podcast_artwork' => array(
        'link_to_player' => 0,
        'field_type' => 'image',
        'media_type' => 'media',
        'preview' => 'mogul_podcast_player_2_player_preview',
        'thumbnail' => 'mogul_podcast_player_2_player_playlist',
        'custom' => '',
      ),
      'field_mogul_podcast_file' => array(
        'link_to_player' => 0,
        'field_type' => 'media',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'title' => array(
        'link_to_player' => 0,
        'field_type' => 'title',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
    ),
  );
  return $options;
}

/**
 * Implements hook_node_info().
 */
function mogul_podcast_node_info() {
  $items = array(
    'mogul_podcast' => array(
      'name' => t('Podcast'),
      'base' => 'node_content',
      'description' => t('Use <em>Podcast</em> to upload an audio or video file for users to listen to or watch in a media player. It also adds the file to an iTunes compatible feed to push your content to iTunes.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
