<?php
/**
 * @file
 * mogul_podcast.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function mogul_podcast_taxonomy_default_vocabularies() {
  return array(
    'mogul_itunes_keywords' => array(
      'name' => 'iTunes Keywords',
      'machine_name' => 'mogul_itunes_keywords',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
