<?php
/**
 * @file
 * mogul_podcast.features.field.inc
 */

/**
 * Implements hook_field_default_fields().
 */
function mogul_podcast_field_default_fields() {
  $fields = array();

  // Exported field: 'node-mogul_podcast-body'.
  $fields['node-mogul_podcast-body'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(
        0 => 'node',
      ),
      'field_name' => 'body',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'locked' => '0',
      'module' => 'text',
      'settings' => array(),
      'translatable' => '0',
      'type' => 'text_with_summary',
    ),
    'field_instance' => array(
      'bundle' => 'mogul_podcast',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => '2',
        ),
        'teaser' => array(
          'label' => 'hidden',
          'module' => 'text',
          'settings' => array(
            'trim_length' => 600,
          ),
          'type' => 'text_summary_or_trimmed',
          'weight' => '0',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'body',
      'label' => 'Body',
      'required' => FALSE,
      'settings' => array(
        'display_summary' => TRUE,
        'text_processing' => 1,
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'module' => 'text',
        'settings' => array(
          'rows' => 20,
          'summary_rows' => 5,
        ),
        'type' => 'text_textarea_with_summary',
        'weight' => '4',
      ),
    ),
  );

  // Exported field: 'node-mogul_podcast-field_mogul_podcast_5star'.
  $fields['node-mogul_podcast-field_mogul_podcast_5star'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_mogul_podcast_5star',
      'foreign keys' => array(),
      'indexes' => array(),
      'locked' => '0',
      'module' => 'fivestar',
      'settings' => array(
        'axis' => 'vote',
      ),
      'translatable' => '0',
      'type' => 'fivestar',
    ),
    'field_instance' => array(
      'bundle' => 'mogul_podcast',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'inline',
          'module' => 'fivestar',
          'settings' => array(
            'expose' => 1,
            'style' => 'average',
            'text' => 'none',
            'widget' => array(
              'fivestar_widget' => 'sites/all/modules/fivestar/widgets/oxygen/oxygen.css',
            ),
          ),
          'type' => 'fivestar_formatter_default',
          'weight' => '1',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_mogul_podcast_5star',
      'label' => 'Rating',
      'required' => 0,
      'settings' => array(
        'allow_clear' => 0,
        'mediafront' => array(
          'custom' => '',
          'field_type' => '0',
          'media_type' => 'media',
          'preview' => '0',
          'thumbnail' => '0',
        ),
        'stars' => '5',
        'target' => 'none',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'fivestar',
        'settings' => array(),
        'type' => 'exposed',
        'weight' => '3',
      ),
    ),
  );

  // Exported field: 'node-mogul_podcast-field_mogul_podcast_artwork'.
  $fields['node-mogul_podcast-field_mogul_podcast_artwork'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_mogul_podcast_artwork',
      'foreign keys' => array(
        'fid' => array(
          'columns' => array(
            'fid' => 'fid',
          ),
          'table' => 'file_managed',
        ),
      ),
      'indexes' => array(
        'fid' => array(
          0 => 'fid',
        ),
      ),
      'locked' => '0',
      'module' => 'image',
      'settings' => array(
        'default_image' => 0,
        'uri_scheme' => 'public',
      ),
      'translatable' => '0',
      'type' => 'image',
    ),
    'field_instance' => array(
      'bundle' => 'mogul_podcast',
      'deleted' => '0',
      'description' => 'iTunes prefers square jpg (or png) images that are at least 1400 x 1400 pixels with a RGB color space (CMYK is not supported).',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '3',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '7',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_mogul_podcast_artwork',
      'label' => 'Podcast Episode Artwork',
      'required' => 1,
      'settings' => array(
        'alt_field' => 0,
        'default_image' => 0,
        'file_directory' => '',
        'file_extensions' => 'jpg png',
        'max_filesize' => '',
        'max_resolution' => '',
        'mediafront' => array(
          'custom' => '',
          'field_type' => 'image',
          'media_type' => 'media',
          'preview' => 'mogul_podcast_node_preview',
          'thumbnail' => '0',
        ),
        'min_resolution' => '1400x1400',
        'title_field' => 0,
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'image',
        'settings' => array(
          'imce_filefield_on' => 1,
          'preview_image_style' => 'thumbnail',
          'progress_indicator' => 'throbber',
        ),
        'type' => 'image_image',
        'weight' => '1',
      ),
    ),
  );

  // Exported field: 'node-mogul_podcast-field_mogul_podcast_block'.
  $fields['node-mogul_podcast-field_mogul_podcast_block'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_mogul_podcast_block',
      'foreign keys' => array(),
      'indexes' => array(
        'value' => array(
          0 => 'value',
        ),
      ),
      'locked' => '0',
      'module' => 'list',
      'settings' => array(
        'allowed_values' => array(
          'no' => 'No',
          'yes' => 'Yes',
        ),
        'allowed_values_function' => '',
      ),
      'translatable' => '0',
      'type' => 'list_text',
    ),
    'field_instance' => array(
      'bundle' => 'mogul_podcast',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => 'Do you wan to prevent this podcast from appearing in the iTunes Podcast directory?',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '6',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '4',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_mogul_podcast_block',
      'label' => 'Block Podcast',
      'required' => 1,
      'settings' => array(
        'mediafront' => array(
          'custom' => '',
          'field_type' => '0',
          'media_type' => 'media',
          'preview' => '0',
          'thumbnail' => '0',
        ),
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(),
        'type' => 'options_select',
        'weight' => '7',
      ),
    ),
  );

  // Exported field: 'node-mogul_podcast-field_mogul_podcast_cc'.
  $fields['node-mogul_podcast-field_mogul_podcast_cc'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_mogul_podcast_cc',
      'foreign keys' => array(),
      'indexes' => array(
        'value' => array(
          0 => 'value',
        ),
      ),
      'locked' => '0',
      'module' => 'list',
      'settings' => array(
        'allowed_values' => array(
          'no' => 'No',
          'yes' => 'Yes',
        ),
        'allowed_values_function' => '',
      ),
      'translatable' => '0',
      'type' => 'list_text',
    ),
    'field_instance' => array(
      'bundle' => 'mogul_podcast',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => 'Does this podcast contain embedded closed captioning support?',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '7',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '2',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_mogul_podcast_cc',
      'label' => 'Closed Captioned',
      'required' => 1,
      'settings' => array(
        'mediafront' => array(
          'custom' => '',
          'field_type' => '0',
          'media_type' => 'media',
          'preview' => '0',
          'thumbnail' => '0',
        ),
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(),
        'type' => 'options_select',
        'weight' => '8',
      ),
    ),
  );

  // Exported field: 'node-mogul_podcast-field_mogul_podcast_explicit'.
  $fields['node-mogul_podcast-field_mogul_podcast_explicit'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_mogul_podcast_explicit',
      'foreign keys' => array(),
      'indexes' => array(
        'value' => array(
          0 => 'value',
        ),
      ),
      'locked' => '0',
      'module' => 'list',
      'settings' => array(
        'allowed_values' => array(
          'no' => 'No',
          'yes' => 'Yes',
        ),
        'allowed_values_function' => '',
      ),
      'translatable' => '0',
      'type' => 'list_text',
    ),
    'field_instance' => array(
      'bundle' => 'mogul_podcast',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => 'Do this podcast episode contain explicit material?',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '5',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '5',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_mogul_podcast_explicit',
      'label' => 'Explicit Material',
      'required' => 1,
      'settings' => array(
        'mediafront' => array(
          'custom' => '',
          'field_type' => '0',
          'media_type' => 'media',
          'preview' => '0',
          'thumbnail' => '0',
        ),
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(),
        'type' => 'options_select',
        'weight' => '6',
      ),
    ),
  );

  // Exported field: 'node-mogul_podcast-field_mogul_podcast_file'.
  $fields['node-mogul_podcast-field_mogul_podcast_file'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '3',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_mogul_podcast_file',
      'foreign keys' => array(
        'fid' => array(
          'columns' => array(
            'fid' => 'fid',
          ),
          'table' => 'file_managed',
        ),
      ),
      'indexes' => array(
        'fid' => array(
          0 => 'fid',
        ),
      ),
      'locked' => '0',
      'module' => 'file',
      'settings' => array(
        'display_default' => 0,
        'display_field' => 0,
        'uri_scheme' => 'public',
      ),
      'translatable' => '0',
      'type' => 'file',
    ),
    'field_instance' => array(
      'bundle' => 'mogul_podcast',
      'deleted' => '0',
      'description' => 'This field can contain a video or audio file. iTunes can accept either one but not both in the same field.

<p><strong>Audio Files</strong><br />
For your audio to be accessible to as many browsers/devices as possible, you will have to upload your video file in 2 formats: .mp3 and .oga.  Please upload them is this order. See <a href="http://drupal.org/node/1623604">HTML 5 Audio File Formats and Encoding</a> for more information. </p>

<p><strong>Video Files</strong><br />
For your video to be accessible to as many browsers/devices as possible, you will have to upload your video file in 3 formats: .webm, .ogv and .mp4.  Please upload them is this order. See <a href="http://drupal.org/node/1565532">HTML 5 Video File Formats and Encoding</a> for more information.</p>',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'mediafront',
          'settings' => array(
            'preset' => 'mogul_podcast_node',
          ),
          'type' => 'mediafront_player',
          'weight' => '0',
        ),
        'teaser' => array(
          'label' => 'hidden',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '1',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_mogul_podcast_file',
      'label' => 'Podcast File',
      'required' => 1,
      'settings' => array(
        'description_field' => 0,
        'file_directory' => '',
        'file_extensions' => 'mp3 oga mp4 webm ogv',
        'max_filesize' => '',
        'mediafront' => array(
          'custom' => '',
          'field_type' => 'media',
          'media_type' => 'media',
          'preview' => '0',
          'thumbnail' => '0',
        ),
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'file',
        'settings' => array(
          'imce_filefield_on' => 1,
          'progress_indicator' => 'throbber',
        ),
        'type' => 'file_generic',
        'weight' => '2',
      ),
    ),
  );

  // Exported field: 'node-mogul_podcast-field_mogul_podcast_itunes_order'.
  $fields['node-mogul_podcast-field_mogul_podcast_itunes_order'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_mogul_podcast_itunes_order',
      'foreign keys' => array(),
      'indexes' => array(
        'value' => array(
          0 => 'value',
        ),
      ),
      'locked' => '0',
      'module' => 'list',
      'settings' => array(
        'allowed_values' => array(
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
          5 => '5',
          6 => '6',
          7 => '7',
          8 => '8',
          9 => '9',
          10 => '10',
        ),
        'allowed_values_function' => '',
      ),
      'translatable' => '0',
      'type' => 'list_integer',
    ),
    'field_instance' => array(
      'bundle' => 'mogul_podcast',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => 'You can optionally override the default ordering of this episode in the iTunes store. For example, if you would like an item to appear as the first episode in the podcast, you would choose “1”. If conflicting order values are present in multiple episodes, the store will use default ordering. Note: This does not affect the ordering of your podcasts on your site, only in the iTunes store. Leave setting to -None- to disable overriding the order.',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '8',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '3',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_mogul_podcast_itunes_order',
      'label' => 'iTunes Order',
      'required' => 0,
      'settings' => array(
        'mediafront' => array(
          'custom' => '',
          'field_type' => '0',
          'media_type' => 'media',
          'preview' => '0',
          'thumbnail' => '0',
        ),
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(),
        'type' => 'options_select',
        'weight' => '9',
      ),
    ),
  );

  // Exported field: 'node-mogul_podcast-field_mogul_podcast_keywords'.
  $fields['node-mogul_podcast-field_mogul_podcast_keywords'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '-1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_mogul_podcast_keywords',
      'foreign keys' => array(
        'tid' => array(
          'columns' => array(
            'tid' => 'tid',
          ),
          'table' => 'taxonomy_term_data',
        ),
      ),
      'indexes' => array(
        'tid' => array(
          0 => 'tid',
        ),
      ),
      'locked' => '0',
      'module' => 'taxonomy',
      'settings' => array(
        'allowed_values' => array(
          0 => array(
            'vocabulary' => 'mogul_itunes_keywords',
            'parent' => '0',
          ),
        ),
      ),
      'translatable' => '0',
      'type' => 'taxonomy_term_reference',
    ),
    'field_instance' => array(
      'bundle' => 'mogul_podcast',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => 'Enter a maximum of 12 keywords, separated by commas.',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '4',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '6',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_mogul_podcast_keywords',
      'label' => 'Podcast Keywords',
      'required' => 1,
      'settings' => array(
        'mediafront' => array(
          'custom' => '',
          'field_type' => '0',
          'media_type' => 'media',
          'preview' => '0',
          'thumbnail' => '0',
        ),
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 0,
        'module' => 'taxonomy',
        'settings' => array(
          'autocomplete_path' => 'taxonomy/autocomplete',
          'size' => 60,
        ),
        'type' => 'taxonomy_autocomplete',
        'weight' => '5',
      ),
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Block Podcast');
  t('Body');
  t('Closed Captioned');
  t('Do this podcast episode contain explicit material?');
  t('Do you wan to prevent this podcast from appearing in the iTunes Podcast directory?');
  t('Does this podcast contain embedded closed captioning support?');
  t('Enter a maximum of 12 keywords, separated by commas.');
  t('Explicit Material');
  t('Podcast Episode Artwork');
  t('Podcast File');
  t('Podcast Keywords');
  t('Rating');
  t('This field can contain a video or audio file. iTunes can accept either one but not both in the same field.

<p><strong>Audio Files</strong><br />
For your audio to be accessible to as many browsers/devices as possible, you will have to upload your video file in 2 formats: .mp3 and .oga.  Please upload them is this order. See <a href="http://drupal.org/node/1623604">HTML 5 Audio File Formats and Encoding</a> for more information. </p>

<p><strong>Video Files</strong><br />
For your video to be accessible to as many browsers/devices as possible, you will have to upload your video file in 3 formats: .webm, .ogv and .mp4.  Please upload them is this order. See <a href="http://drupal.org/node/1565532">HTML 5 Video File Formats and Encoding</a> for more information.</p>');
  t('You can optionally override the default ordering of this episode in the iTunes store. For example, if you would like an item to appear as the first episode in the podcast, you would choose “1”. If conflicting order values are present in multiple episodes, the store will use default ordering. Note: This does not affect the ordering of your podcasts on your site, only in the iTunes store. Leave setting to -None- to disable overriding the order.');
  t('iTunes Order');
  t('iTunes prefers square jpg (or png) images that are at least 1400 x 1400 pixels with a RGB color space (CMYK is not supported).');

  return $fields;
}
