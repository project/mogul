<?php
/**
 * @file
 * mogul_audio.features.field.inc
 */

/**
 * Implements hook_field_default_fields().
 */
function mogul_audio_field_default_fields() {
  $fields = array();

  // Exported field: 'node-mogul_audio-body'.
  $fields['node-mogul_audio-body'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(
        0 => 'node',
      ),
      'field_name' => 'body',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'locked' => '0',
      'module' => 'text',
      'settings' => array(),
      'translatable' => '0',
      'type' => 'text_with_summary',
    ),
    'field_instance' => array(
      'bundle' => 'mogul_audio',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => '2',
        ),
        'teaser' => array(
          'label' => 'hidden',
          'module' => 'text',
          'settings' => array(
            'trim_length' => 600,
          ),
          'type' => 'text_summary_or_trimmed',
          'weight' => '0',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'body',
      'label' => 'Body',
      'required' => FALSE,
      'settings' => array(
        'display_summary' => TRUE,
        'text_processing' => 1,
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'module' => 'text',
        'settings' => array(
          'rows' => 20,
          'summary_rows' => 5,
        ),
        'type' => 'text_textarea_with_summary',
        'weight' => '3',
      ),
    ),
  );

  // Exported field: 'node-mogul_audio-field_mogul_audio_5star'.
  $fields['node-mogul_audio-field_mogul_audio_5star'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_mogul_audio_5star',
      'foreign keys' => array(),
      'indexes' => array(),
      'locked' => '0',
      'module' => 'fivestar',
      'settings' => array(
        'axis' => 'vote',
      ),
      'translatable' => '0',
      'type' => 'fivestar',
    ),
    'field_instance' => array(
      'bundle' => 'mogul_audio',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'inline',
          'module' => 'fivestar',
          'settings' => array(
            'expose' => 1,
            'style' => 'average',
            'text' => 'none',
            'widget' => array(
              'fivestar_widget' => 'sites/all/modules/fivestar/widgets/oxygen/oxygen.css',
            ),
          ),
          'type' => 'fivestar_formatter_default',
          'weight' => '1',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_mogul_audio_5star',
      'label' => 'Rating',
      'required' => 0,
      'settings' => array(
        'allow_clear' => 0,
        'mediafront' => array(
          'custom' => '',
          'field_type' => '0',
          'media_type' => 'media',
          'preview' => '0',
          'thumbnail' => '0',
        ),
        'stars' => '5',
        'target' => 'none',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'fivestar',
        'settings' => array(),
        'type' => 'exposed',
        'weight' => '4',
      ),
    ),
  );

  // Exported field: 'node-mogul_audio-field_mogul_audio_file'.
  $fields['node-mogul_audio-field_mogul_audio_file'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_mogul_audio_file',
      'foreign keys' => array(
        'fid' => array(
          'columns' => array(
            'fid' => 'fid',
          ),
          'table' => 'file_managed',
        ),
      ),
      'indexes' => array(
        'fid' => array(
          0 => 'fid',
        ),
      ),
      'locked' => '0',
      'module' => 'file',
      'settings' => array(
        'display_default' => 0,
        'display_field' => 0,
        'uri_scheme' => 'public',
      ),
      'translatable' => '0',
      'type' => 'file',
    ),
    'field_instance' => array(
      'bundle' => 'mogul_audio',
      'deleted' => '0',
      'description' => 'For your audio to be accessible to as many browsers/devices as possible, you will have to upload your video file in 2 formats: .mp3 and .oga.  Please upload them is this order. See <a href="http://drupal.org/node/1623604" target="_blank">HTML 5 Audio File Formats and Encoding</a> for more information. </p>',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'mediafront',
          'settings' => array(
            'preset' => 'mogul_audio_node',
          ),
          'type' => 'mediafront_player',
          'weight' => '0',
        ),
        'teaser' => array(
          'label' => 'hidden',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '1',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_mogul_audio_file',
      'label' => 'Audio File',
      'required' => 1,
      'settings' => array(
        'description_field' => 0,
        'file_directory' => '',
        'file_extensions' => 'mp3 ogv ogg',
        'max_filesize' => '',
        'mediafront' => array(
          'custom' => '',
          'field_type' => 'media',
          'media_type' => 'media',
          'preview' => '0',
          'thumbnail' => '0',
        ),
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'file',
        'settings' => array(
          'imce_filefield_on' => 1,
          'progress_indicator' => 'throbber',
        ),
        'type' => 'file_generic',
        'weight' => '1',
      ),
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Audio File');
  t('Body');
  t('For your audio to be accessible to as many browsers/devices as possible, you will have to upload your video file in 2 formats: .mp3 and .oga.  Please upload them is this order. See <a href="http://drupal.org/node/1623604" target="_blank">HTML 5 Audio File Formats and Encoding</a> for more information. </p>');
  t('Rating');

  return $fields;
}
