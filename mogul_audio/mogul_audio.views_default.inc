<?php
/**
 * @file
 * mogul_audio.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function mogul_audio_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'mogul_audio_favorites';
  $view->description = 'Provides a tab on all user\'s profile pages containing favortie audio for that user.';
  $view->tag = 'mogul, audio';
  $view->base_table = 'node';
  $view->human_name = 'Mogul: Audio (Favorites)';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Favorite Audio';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '12';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'This user has not \'Favorited\' any content yet.';
  $handler->display->display_options['empty']['area']['format'] = 'full_html';
  /* Relationship: Flags: mogul_audio_favorites */
  $handler->display->display_options['relationships']['flag_content_rel']['id'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['flag_content_rel']['field'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['flag'] = 'mogul_audio_favorites';
  /* Field: Content: Audio File */
  $handler->display->display_options['fields']['field_mogul_audio_file']['id'] = 'field_mogul_audio_file';
  $handler->display->display_options['fields']['field_mogul_audio_file']['table'] = 'field_data_field_mogul_audio_file';
  $handler->display->display_options['fields']['field_mogul_audio_file']['field'] = 'field_mogul_audio_file';
  $handler->display->display_options['fields']['field_mogul_audio_file']['label'] = '';
  $handler->display->display_options['fields']['field_mogul_audio_file']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_mogul_audio_file']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_mogul_audio_file']['type'] = 'mediafront_player';
  $handler->display->display_options['fields']['field_mogul_audio_file']['settings'] = array(
    'preset' => 'mogul_audio_grid_small',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Flags: Flag link */
  $handler->display->display_options['fields']['ops']['id'] = 'ops';
  $handler->display->display_options['fields']['ops']['table'] = 'flag_content';
  $handler->display->display_options['fields']['ops']['field'] = 'ops';
  $handler->display->display_options['fields']['ops']['relationship'] = 'flag_content_rel';
  $handler->display->display_options['fields']['ops']['label'] = '';
  $handler->display->display_options['fields']['ops']['element_label_colon'] = FALSE;
  /* Field: Content: Rating */
  $handler->display->display_options['fields']['field_mogul_audio_5star']['id'] = 'field_mogul_audio_5star';
  $handler->display->display_options['fields']['field_mogul_audio_5star']['table'] = 'field_data_field_mogul_audio_5star';
  $handler->display->display_options['fields']['field_mogul_audio_5star']['field'] = 'field_mogul_audio_5star';
  $handler->display->display_options['fields']['field_mogul_audio_5star']['label'] = '';
  $handler->display->display_options['fields']['field_mogul_audio_5star']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_mogul_audio_5star']['click_sort_column'] = 'rating';
  $handler->display->display_options['fields']['field_mogul_audio_5star']['settings'] = array(
    'widget' => array(
      'fivestar_widget' => 'sites/all/modules/fivestar/widgets/oxygen/oxygen.css',
    ),
    'expose' => 1,
    'style' => 'average',
    'text' => 'none',
  );
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Flags: User uid */
  $handler->display->display_options['arguments']['uid']['id'] = 'uid';
  $handler->display->display_options['arguments']['uid']['table'] = 'flag_content';
  $handler->display->display_options['arguments']['uid']['field'] = 'uid';
  $handler->display->display_options['arguments']['uid']['relationship'] = 'flag_content_rel';
  $handler->display->display_options['arguments']['uid']['default_action'] = 'empty';
  $handler->display->display_options['arguments']['uid']['default_argument_type'] = 'user';
  $handler->display->display_options['arguments']['uid']['default_argument_options']['user'] = FALSE;
  $handler->display->display_options['arguments']['uid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['uid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['uid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['uid']['specify_validation'] = TRUE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'mogul_audio' => 'mogul_audio',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['path'] = 'user/%/favorite-audio';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Favorite Audio';
  $handler->display->display_options['menu']['description'] = 'View favorite audio.';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $export['mogul_audio_favorites'] = $view;

  $view = new view();
  $view->name = 'mogul_audio_grid_small';
  $view->description = 'A grid display of small audio players for each item item in the view for the Audio content type.';
  $view->tag = 'mogul, audio';
  $view->base_table = 'node';
  $view->human_name = 'Mogul: Audio (Grid Small Players)';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Audio Grid Small';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '12';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Flags: mogul_audio_favorites */
  $handler->display->display_options['relationships']['flag_content_rel']['id'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['flag_content_rel']['field'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['required'] = 0;
  $handler->display->display_options['relationships']['flag_content_rel']['flag'] = 'mogul_audio_favorites';
  /* Field: Content: Audio File */
  $handler->display->display_options['fields']['field_mogul_audio_file']['id'] = 'field_mogul_audio_file';
  $handler->display->display_options['fields']['field_mogul_audio_file']['table'] = 'field_data_field_mogul_audio_file';
  $handler->display->display_options['fields']['field_mogul_audio_file']['field'] = 'field_mogul_audio_file';
  $handler->display->display_options['fields']['field_mogul_audio_file']['label'] = '';
  $handler->display->display_options['fields']['field_mogul_audio_file']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_mogul_audio_file']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_mogul_audio_file']['type'] = 'mediafront_player';
  $handler->display->display_options['fields']['field_mogul_audio_file']['settings'] = array(
    'preset' => 'mogul_audio_grid_small',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Flags: Flag link */
  $handler->display->display_options['fields']['ops']['id'] = 'ops';
  $handler->display->display_options['fields']['ops']['table'] = 'flag_content';
  $handler->display->display_options['fields']['ops']['field'] = 'ops';
  $handler->display->display_options['fields']['ops']['relationship'] = 'flag_content_rel';
  $handler->display->display_options['fields']['ops']['label'] = '';
  $handler->display->display_options['fields']['ops']['element_label_colon'] = FALSE;
  /* Field: Content: Rating */
  $handler->display->display_options['fields']['field_mogul_audio_5star']['id'] = 'field_mogul_audio_5star';
  $handler->display->display_options['fields']['field_mogul_audio_5star']['table'] = 'field_data_field_mogul_audio_5star';
  $handler->display->display_options['fields']['field_mogul_audio_5star']['field'] = 'field_mogul_audio_5star';
  $handler->display->display_options['fields']['field_mogul_audio_5star']['label'] = '';
  $handler->display->display_options['fields']['field_mogul_audio_5star']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_mogul_audio_5star']['click_sort_column'] = 'rating';
  $handler->display->display_options['fields']['field_mogul_audio_5star']['settings'] = array(
    'widget' => array(
      'fivestar_widget' => 'sites/all/modules/fivestar/widgets/oxygen/oxygen.css',
    ),
    'expose' => 1,
    'style' => 'average',
    'text' => 'none',
  );
  /* Sort criterion: Content: Sticky */
  $handler->display->display_options['sorts']['sticky']['id'] = 'sticky';
  $handler->display->display_options['sorts']['sticky']['table'] = 'node';
  $handler->display->display_options['sorts']['sticky']['field'] = 'sticky';
  $handler->display->display_options['sorts']['sticky']['order'] = 'DESC';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'mogul_audio' => 'mogul_audio',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['path'] = 'audio/grid/small';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Audio Grid Small';
  $handler->display->display_options['menu']['description'] = 'Listen to our audio.';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $export['mogul_audio_grid_small'] = $view;

  return $export;
}
