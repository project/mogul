<?php
/**
 * @file
 * mogul_audio.features.inc
 */

/**
 * Implements hook_views_api().
 */
function mogul_audio_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_flag_default_flags().
 */
function mogul_audio_flag_default_flags() {
  $flags = array();
  // Exported flag: "Favorite Audio".
  $flags['mogul_audio_favorites'] = array(
    'content_type' => 'node',
    'title' => 'Favorite Audio',
    'global' => '0',
    'types' => array(
      0 => 'mogul_audio',
    ),
    'flag_short' => 'Add to Favorites',
    'flag_long' => 'Add [node:title] to your favorites.',
    'flag_message' => '[node:title] has now been added to your favorites.',
    'unflag_short' => 'Remove from Favorites',
    'unflag_long' => 'Remove [node:title] from your favorites.',
    'unflag_message' => '[node:title] has now been removed from your favorites.',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '2',
      ),
      'unflag' => array(
        0 => '2',
      ),
    ),
    'weight' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_on_page' => 1,
    'show_on_teaser' => 1,
    'show_contextual_link' => 0,
    'i18n' => 0,
    'api_version' => 2,
    'module' => 'mogul_audio',
    'locked' => array(
      0 => 'name',
    ),
  );
  return $flags;

}

/**
 * Implements hook_mediafront_default_presets().
 */
function mogul_audio_mediafront_default_presets() {
  $items = array(
    'mogul_audio_grid_small' => array(
      'name' => 'mogul_audio_grid_small',
      'description' => 'The <b>small grid</b> player that displays a player for <b>each node</b> in the view for the <b>Audio</b> content type.',
      'player' => 'osmplayer',
      'connect' => array(
        'iscontroller' => array(
          0 => '0',
        ),
        'isplaylist' => array(
          0 => '0',
        ),
      ),
      'settings' => array(
        'debug' => 0,
        'volume' => '80',
        'wmode' => 'transparent',
        'preload' => TRUE,
        'autoplay' => 0,
        'autoload' => 0,
        'scrollMode' => 'auto',
        'scrollSpeed' => '20',
        'showPlaylist' => 1,
        'vertical' => '1',
        'node' => array(),
        'playlist' => '',
        'pageLimit' => '10',
        'preset' => '',
        'autoNext' => 1,
        'shuffle' => 0,
        'loop' => 0,
        'logo' => '/sites/all/modules/mediafront/players/osmplayer/player/logo.png',
        'swfplayer' => '/sites/all/modules/mediafront/players/osmplayer/player/minplayer/flash/minplayer.swf',
        'link' => 'http://www.mediafront.org',
        'width' => '100%',
        'height' => '',
        'template' => 'default',
        'playlistOnly' => 0,
        'disablePlaylist' => 0,
        'controllerOnly' => 1,
        'volumeVertical' => 1,
        'plugins' => array(
          'timeline_indicator' => 0,
        ),
        'theme' => 'dark-hive',
        'showWhenEmpty' => 0,
        'showController' => 1,
        'prereel' => '',
        'postreel' => '',
        'player_settings__active_tab' => 'edit-player-settings-presentation',
      ),
    ),
    'mogul_audio_node' => array(
      'name' => 'mogul_audio_node',
      'description' => 'The <b>node</b> player  for the <b>Audio</b> content type.',
      'player' => 'osmplayer',
      'connect' => array(
        'iscontroller' => array(
          0 => '0',
        ),
        'isplaylist' => array(
          0 => '0',
        ),
      ),
      'settings' => array(
        'debug' => 0,
        'volume' => '80',
        'wmode' => 'transparent',
        'preload' => TRUE,
        'autoplay' => 0,
        'autoload' => 0,
        'scrollMode' => 'none',
        'scrollSpeed' => '20',
        'showPlaylist' => 1,
        'vertical' => '1',
        'node' => array(),
        'playlist' => '',
        'pageLimit' => '10',
        'preset' => '',
        'autoNext' => 0,
        'shuffle' => 0,
        'loop' => 0,
        'logo' => '/sites/all/modules/mediafront/players/osmplayer/player/logo.png',
        'swfplayer' => '/sites/all/modules/mediafront/players/osmplayer/player/minplayer/flash/minplayer.swf',
        'link' => 'http://www.mediafront.org',
        'width' => '100%',
        'height' => '',
        'template' => 'default',
        'playlistOnly' => 0,
        'disablePlaylist' => 0,
        'controllerOnly' => 1,
        'volumeVertical' => 1,
        'theme' => 'dark-hive',
        'showWhenEmpty' => 0,
        'showController' => 1,
        'prereel' => '',
        'postreel' => '',
      ),
    ),
  );
  return $items;
}

/**
 * Implements hook_mediafront_views_default_options().
 */
function mogul_audio_mediafront_views_default_options() {
  $options = array(
    'mogul_audio_favorites' => array(
      'nid' => array(
        'link_to_player' => 0,
        'field_type' => '0',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'uid' => array(
        'link_to_player' => 0,
        'field_type' => '0',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'area' => array(
        'link_to_player' => 0,
        'field_type' => '0',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'field_mogul_audio_file' => array(
        'link_to_player' => 0,
        'field_type' => 'media',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'field_mogul_audio_5star' => array(
        'link_to_player' => 0,
        'field_type' => '0',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
      'ops' => array(
        'link_to_player' => 0,
        'field_type' => '0',
        'media_type' => 'media',
        'preview' => '0',
        'thumbnail' => '0',
        'custom' => '',
      ),
    ),
  );
  return $options;
}

/**
 * Implements hook_node_info().
 */
function mogul_audio_node_info() {
  $items = array(
    'mogul_audio' => array(
      'name' => t('Audio'),
      'base' => 'node_content',
      'description' => t('Use <em>Audio</em> to upload an audio file for users to listen to in a media player.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
